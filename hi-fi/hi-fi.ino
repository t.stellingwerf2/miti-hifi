#include "Arduino.h"
#include "DFRobotDFPlayerMini.h"
#include <Stepper.h>
#include <Timeout.h>

#define setbit(data,b) (data|=(1<<b))
#define clrbit(data,b) (data&=~(1<<b))
#define coilImage_1 9;
#define coilImage_2 10;
#define coilImage_3 11;
#define coilImage_4 12;
#define coilTask_1 14;
#define coilTask_2 15;
#define coilTask_3 16;
#define coilTask_4 17;

//Decade and year
uint8_t encoderDecade = 0;
uint8_t encoderYear = 0;
const uint8_t code8PinDecade = 1;
const uint8_t code4PinDecade = 2;
const uint8_t code2PinDecade = 3;
const uint8_t code1PinDecade = 4;
const uint8_t code8PinYear = 5;
const uint8_t code4PinYear = 6;
const uint8_t code2PinYear = 7;
const uint8_t code1PinYear = 8;

//MP3 player
DFRobotDFPlayerMini mp3Player;

//Year image
Stepper stepperImage(4096, coilImage_1, coilImage_2, coilImage_3, coilImage_4);
unsigned long timestampImage;

//Next button
int nextPin = 13;

//Task
TimeOut taskTimeout;
Stepper stepperTask(4096, coilTask_1, coilTask_2, coilTask_3, coilTask_4);
unsigned long timestampTask;

void setup() {
  // put your setup code here, to run once:
  //Decade and year setup
  pinMode(code8PinDecade, INPUT);
  pinMode(code4PinDecade, INPUT);
  pinMode(code2PinDecade, INPUT);
  pinMode(code1PinDecade, INPUT);
  pinMode(code8PinYear, INPUT);
  pinMode(code4PinYear, INPUT);
  pinMode(code2PinYear, INPUT);
  pinMode(code1PinYear, INPUT);
  uint8_t previousYear = yearEncoder();

  //MP3 setup
  mp3Player.volume(10);
  mp3Player.EQ(DFPLAYER_EQ_NORMAL);
  mp3Player.outputDevice(DFPLAYER_DEVICE_SD);
  int previousSong = -1;

  //Image setup
  stepperImage.setSpeed(4);

  //Next button setup
  pinMode(nextPin, INPUT);

  //Task setup
  stepperTask.setSpeed(4);
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(1000);

  //Read year
  const uint8_t year = yearEncoder();
  if (year !== previousYear) {
    previousYear = year;
    //Play song
    playSong(year);

    //Turn image rotator
    setImage(year);
    checkImageTimeout();

    //Play next song when next is pressed
    taskTimeout.timeOut(30000, nextSong(year));

    //Turn task rotator
    setTask();
    checkTaskTimeout();
  }
}

uint8_t yearEncoder() {
  //Get decade bit code
  if (digitalRead(code8PinDecade) == HIGH) {
    setbit(encoderDecade, 3);
  } else {
    clrbit(encoderDecade, 3);
  }
  if (digitalRead(code4PinDecade) == HIGH) {
    setbit(encoderDecade, 2);
  } else {
    clrbit(encoderDecade, 2);
  }
  if (digitalRead(code2PinDecade) == HIGH) {
    setbit(encoderDecade, 1);
  } else {
    clrbit(encoderDecade, 1);
  }
  if (digitalRead(code1PinDecade) == HIGH) {
    setbit(encoderDecade, 0);
  } else {
    clrbit(encoderDecade, 0);
  }

  //Get year bit code
  if (digitalRead(code8PinYear) == HIGH) {
    setbit(encoderYear, 3);
  } else {
    clrbit(encoderYear, 3);
  }
  if (digitalRead(code4PinYear) == HIGH) {
    setbit(encoderYear, 2);
  } else {
    clrbit(encoderYear, 2);
  }
  if (digitalRead(code2PinYear) == HIGH) {
    setbit(encoderYear, 1);
  } else {
    clrbit(encoderYear, 1);
  }
  if (digitalRead(code1PinYear) == HIGH) {
    setbit(encoderYear, 0);
  } else {
    clrbit(encoderYear, 0);
  }

  //Combine decade and year to actual year number
  return 1940 + (10 * floor(encoderDecade / 1.6)) + floor(encoderYear / 1.6));
}

void playSong(uint8_t year) {
  uint8_t songNumber;
  if (year == previousYear) {
    while (songNumber == previousSong) {
      songNumber = (year - 1940) * 3 + random(1, 3);
    }
  } else {
    songNumber = (year - 1940) * 3 + random(1, 3);
  }

  previousSong = songNumber;

  mp3Player.play(songNumber);
}

void setImage(uint8_t year) {
  stepperImage.step(yearToImage(year) - yearToImage(previousYear));

  timestampImage = millis();
}

int yearToImagePos(uint8_t year) {
  int yearIncrement = floor(year / 10) - 194;
  return yearIncrement * 409.6;
}

void checkImageTimeout() {
  if ((millis() - timestampImage) > 1000) {
    digitalWrite(coilImage_1, LOW);
    digitalWrite(coilImage_2, LOW);
    digitalWrite(coilImage_3, LOW);
    digitalWrite(coilImage_4, LOW);
  }
}

void nextSong(uint8_t year) {
  int val = digitalRead(nextPin);
  if (val == HIGH) {
    playSong(year);
  }
}

void setTask() {
  stepperTask.step(random(1, 3) * (4096 / 8));

  timestampTask = millis();
}

void checkTaskTimeout() {
  if ((millis() - timestampTask) > 1000) {
    digitalWrite(coilTask_1, LOW);
    digitalWrite(coilTask_2, LOW);
    digitalWrite(coilTask_3, LOW);
    digitalWrite(coilTask_4, LOW);
  }
}